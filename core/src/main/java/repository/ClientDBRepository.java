package repository;

import domain.Client;
import domain.validators.ClientValidator;
import domain.validators.ValidatorException;

import java.sql.*;
import java.util.Optional;

public class ClientDBRepository implements Repository<Long, Client> {

    private String _dbName;
    private String _USER;
    private String _PASSWORD;
    private Connection _connection;
    private Statement _statement;
    private InMemoryRepository<Long, Client> _entities;


    public ClientDBRepository(ClientValidator validator, String _dbName, String _USER, String _PASSWORD) throws SQLException, ValidatorException {
        this._dbName = _dbName;
        this._USER = _USER;
        this._PASSWORD = _PASSWORD;
        _entities = new InMemoryRepository<>(validator);
        _getClientsFromDataBase();
    }

    /**
     * Create a connection to the database and re-insert the data from the
     * data base into the in-memory repository. At the end, close the connection.
     * @throws SQLException - if the connection can't be done or we can't read data from the data base
     */
    private void _getClientsFromDataBase() throws SQLException, ValidatorException {
        String getAll = "SELECT * FROM clients;";
        _connection = DriverManager.getConnection(this._dbName, this._USER, this._PASSWORD);
        // Create the connection
        _statement = _connection.createStatement();
        // Get the result set for the statement
        // If we have an error with the result set, it will be closed at the end
        try (ResultSet resultSet = _statement.executeQuery(getAll)) {
            // While we can get a book data, create a new one and add it to the entities array
            while (resultSet.next()) {
                Client b = _createClient(resultSet);
                _entities.save(b);
            }
        } catch (SQLException e) {
            throw new SQLException("Error: Can't read data from the database");
        } catch (ValidatorException e) {
            throw new ValidatorException("Error: Invalid book inserted");
        } finally {
            _statement.close();
            _connection.close();
        }
    }

    /**
     * Get the data from the result set and read one book from the database.
     * @param rs: ResultSet - the result set got from the query
     * @return Book - the book created with the
     * @throws SQLException - if the book can't be created with the information from the database
     */
    private Client _createClient (ResultSet rs) throws SQLException {
        try {
            Long id = rs.getLong("c_id");
            String name = rs.getString("client_name");
            String street = rs.getString("client_street");
            return new Client(id, name, street);
        } catch (SQLException e) {
            throw new SQLException("ERROR: Couldn't create Book from the database");
        }
    }


    /**
     * Find a client by and id
     * @param aLong: Long - The id of the client
     * @return Client - the client found
     * @throws Exception - if the database can't be accessed.
     */
    @Override
    public Optional<Client> findOne(Long aLong) throws Exception {
        _getClientsFromDataBase();
        return _entities.findOne(aLong);
    }

    /**
     * Return a list of all the clients
     * @return Iterable<Client> - the list of clients
     * @throws Exception - If the connection can't be created
     */
    @Override
    public Iterable<Client> findAll() throws Exception {
        _getClientsFromDataBase();
        return _entities.findAll();
    }

    /**
     * Save a new client to the data base
     * @param entity: Client - the client to be saved
     *            must not be null.
     * @return - the client, if the client is created
     * @throws Exception - If the connection can't be created
     */
    @Override
    public Optional<Client> save(Client entity) throws Exception {
        _connection = DriverManager.getConnection(this._dbName, this._USER, this._PASSWORD);
        _statement = _connection.createStatement();
        String sqlStatement = "INSERT INTO clients(client_name, client_street) VALUES ('"
                + entity.get_name() + "', '"
                + entity.get_address() + "');";
        _statement.executeUpdate(sqlStatement);
        _statement.close();
        _connection.close();
        _getClientsFromDataBase();
        return _entities.findOne(entity.getID());

    }

    /**
     * Remove an element from the database, by a given id
     * @param aLong: Long - the id of the element to be removed
     * @return The client, if not removed, null otherwise
     * @throws Exception - if the connection to remove the client can't be created
     */
    @Override
    public Optional<Client> delete(Long aLong) throws Exception {
        _connection = DriverManager.getConnection(_dbName, _USER, _PASSWORD);
        _statement = _connection.createStatement();
        String sqlStatement = "DELETE FROM clients WHERE c_id = " + aLong.toString();
        _statement.executeUpdate(sqlStatement);
        // To refresh the values that are into the database
        _getClientsFromDataBase();
        _statement.close();
        _connection.close();
        return _entities.findOne(aLong);
    }

    /**
     * Update a client with the new values
     * @param entity: client to be updated, with the new values
     *            must not be null.
     * @return Client - with the new values
     * @throws Exception - if the connection can't be updated
     */
    @Override
    public Optional<Client> update(Client entity) throws Exception {
        _connection = DriverManager.getConnection(_dbName, _USER, _PASSWORD);
        _statement = _connection.createStatement();
        String sqlStatement = "UPDATE clients SET " +
                "client_name='" + entity.get_name() +
                "', client_street='" + entity.get_address() +
                "' WHERE c_id = " + entity.getID() +
                ";";
        _statement.executeUpdate(sqlStatement);
        _statement.close();
        _connection.close();
        return _entities.findOne(entity.getID());
    }
}
