package repository;

import domain.Book;
import domain.validators.Validator;
import domain.validators.ValidatorException;

import java.sql.*;
import java.util.Optional;

public class BookDBRepository implements Repository<Long, Book>{
    private String _dbName;
    private String _USER;
    private String _PASSWORD;
    private Connection _connection;
    private Statement _statement;
    private InMemoryRepository<Long, Book> _entities;

    public BookDBRepository(Validator<Book> _validator, String _dbName, String _USER, String _PASSWORD) throws SQLException, ValidatorException {
        this._dbName = _dbName;
        this._USER = _USER;
        this._PASSWORD = _PASSWORD;
        _entities = new InMemoryRepository<>(_validator);
        _getBooksFromDataBase();
    }

    /**
     * Create a connection to the database and re-insert the data from the
     * data base into the in-memory repository. At the end, close the connection.
     * @throws SQLException - if the connection can't be done or we can't read data from the data base
     */
    private void _getBooksFromDataBase() throws SQLException, ValidatorException {
        String getAll = "SELECT * FROM books;";
        _connection = DriverManager.getConnection(this._dbName, this._USER, this._PASSWORD);
        // Create the connection
        _statement = _connection.createStatement();
        // Get the result set for the statement
        // If we have an error with the result set, it will be closed at the end
        try (ResultSet resultSet = _statement.executeQuery(getAll)) {
            // While we can get a book data, create a new one and add it to the entities array
            while (resultSet.next()) {
                Book b = _createBook(resultSet);
                _entities.save(b);
            }
        } catch (SQLException e) {
            throw new SQLException("Error: Can't read data from the database");
        } catch (ValidatorException e) {
            throw new ValidatorException("Error: Invalid book inserted");
        } finally {
            _statement.close();
            _connection.close();
        }
    }

    /**
     * Get the data from the result set and read one book from the database.
     * @param rs: ResultSet - the result set got from the query
     * @return Book - the book created with the
     * @throws SQLException - if the book can't be created with the information from the database
     */
    private Book _createBook (ResultSet rs) throws SQLException {
        try {
            Long id = rs.getLong("b_id");
            String name = rs.getString("book_name");
            String author = rs.getString("book_author");
            int price = rs.getInt("price");
            return new Book(id, name, author, price);
        } catch (SQLException e) {
            throw new SQLException("ERROR: Couldn't create Book from the database");
        }
    }

    /**
     * Find a book, given an id.
     * @param aLong: Long - the id of the book
     * @return Book - the book found, null otherwise
     * @throws SQLException - if the values can't be read from the database
     * @throws ValidatorException - if the values from the database can't define a valid book
     */
    @Override
    public Optional<Book> findOne(Long aLong) throws SQLException, ValidatorException {
        _getBooksFromDataBase();
        return _entities.findOne(aLong);
    }

    /**
     * Return all of the books in the database
     * @return an iterable over the books from the database
     * @throws Exception - If the books can't be accessed from the database
     */
    @Override
    public Iterable<Book> findAll() throws Exception {
        _getBooksFromDataBase();
        return _entities.findAll();
    }

    /**
     * Save a given book in the database and in the in memory repository.
     * @param entity: Book - the book to be saved
     *            must not be null.
     * @return Book - if the book can be saved, null otherwise
     * @throws Exception - If the book can't be inserted into the database,
     *      because of connection problem or validation problems
     */
    @Override
    public Optional<Book> save(Book entity) throws Exception {
        _connection = DriverManager.getConnection(this._dbName, this._USER, this._PASSWORD);
        _statement = _connection.createStatement();
        String sqlStatement = "INSERT INTO books(book_name, book_author, price) VALUES ('"
                + entity.getName() + "', '"
                + entity.getAuthor() + "', '"
                + entity.getPrice() + "');";
        _statement.executeUpdate(sqlStatement);
        _statement.close();
        _connection.close();
        _getBooksFromDataBase();
        return _entities.findOne(entity.getID());
    }

    /**
     * Remove a value from the database, given a given id
     * @param aLong: Long - the id of the book to remove
     * @return the book, if removed, null otherwise
     * @throws Exception - if the connection to remove the book can't be done
     */
    @Override
    public Optional<Book> delete(Long aLong) throws Exception {
        _connection = DriverManager.getConnection(_dbName, _USER, _PASSWORD);
        _statement = _connection.createStatement();
        String sqlStatement = "DELETE FROM books WHERE b_id = " + aLong.toString();
        _statement.executeUpdate(sqlStatement);
        // To refresh the values that are into the database
        _getBooksFromDataBase();
        _statement.close();
        _connection.close();
        return _entities.findOne(aLong);
    }

    /**
     * Update a book with it's new values
     * @param entity
     *            must not be null.
     * @return Book - if the book can be updated, null otherwise
     * @throws Exception - if the connection can't be made or there is an error with the update operation.
     */
    @Override
    public Optional<Book> update(Book entity) throws Exception {
        _connection = DriverManager.getConnection(_dbName, _USER, _PASSWORD);
        _statement = _connection.createStatement();
        String sqlStatement = "UPDATE books SET " +
                "book_name='" + entity.getName() +
                "', book_author='" + entity.getAuthor() +
                "', price=" + entity.getPrice() +
                " WHERE b_id = " + entity.getID() +
                ";";
        _statement.executeUpdate(sqlStatement);
        _statement.close();
        _connection.close();
        return _entities.findOne(entity.getID());
    }
}
