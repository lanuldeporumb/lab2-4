package repository;

import domain.Client;
import domain.IDGenerator;
import domain.validators.Validator;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.IOException;
import java.util.Optional;

public class ClientXMLRepository implements Repository<Long, Client>{
    private Repository<Long, Client> _inMemorySave;
    private Validator<Client> _clientValidator;
    private String _filePath;

    public ClientXMLRepository(String _filePath, Validator<Client> validator) throws IOException {
        this._clientValidator = validator;
        this._inMemorySave = new InMemoryRepository<>(_clientValidator);
        this._filePath = _filePath;
        try {
            _loadDataFromXML();
        } catch (Exception e) {
            throw new IOException("Error: Invalid path to the client XML file");
        }
    }

    @Override
    public Optional<Client> findOne(Long aLong) throws Exception {
        return _inMemorySave.findOne(aLong);
    }

    @Override
    public Iterable<Client> findAll() throws Exception {
        return _inMemorySave.findAll();
    }

    @Override
    public Optional<Client> save(Client entity) throws Exception {
        _clientValidator.validate(entity);
        Optional<Client> a = _inMemorySave.save(entity);
        _writeEntitiesToXML();
        return a;
    }

    @Override
    public Optional<Client> delete(Long aLong) throws Exception {
        Optional<Client> a = _inMemorySave.delete(aLong);
        _writeEntitiesToXML();
        return a;
    }

    @Override
    public Optional<Client> update(Client entity) throws Exception {
        Optional<Client> a = _inMemorySave.update(entity);
        _writeEntitiesToXML();
        return a;
    }

    /**
     * Load the information from the XML file repository
     * @throws Exception - if the file can't be opened or read
     */
    private void _loadDataFromXML() throws Exception {
        DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
        Document xmlDocument = documentBuilder.parse(_filePath);
        // Read the root of the XML file
        Element root = xmlDocument.getDocumentElement();
        NodeList children = root.getChildNodes();
        for (int i = 0; i < children.getLength(); i++) {
            Node child = children.item(i);
            if (child instanceof Element) {
                Element bookElement = ((Element) child);
                String name = bookElement.getChildNodes().item(0).getTextContent();
                String address = bookElement.getChildNodes().item(1).getTextContent();
                // Create the book
                _inMemorySave.save(new Client(IDGenerator.GenerateID(), name, address));
            }
        }
    }

    /**
     * Write the given book to the XML file.
     * @param client: Client - the book to add to the XML file
     * @throws Exception - if the client can't be written to the file
     */
    private void _writeToXML(Client client) throws Exception {
        // Initiate the elements needed to create a builder for the XML file
        DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
        Document xmlDocument = documentBuilder.parse(_filePath);
        Element root = xmlDocument.getDocumentElement();
        Element clientToSave = xmlDocument.createElement("Client");
        _addChildToElement(xmlDocument, clientToSave, "name", client.get_name());
        _addChildToElement(xmlDocument, clientToSave, "address", client.get_address());
        root.appendChild(clientToSave);
        Transformer transformer = TransformerFactory.newInstance().newTransformer();
        transformer.transform(new DOMSource(root), new StreamResult(_filePath));
    }

    private void _writeEntitiesToXML() {
        try {
            _inMemorySave.findAll().forEach(client -> {
                try {
                    _writeToXML(client);
                } catch (Exception e) {
                    throw new RuntimeException("Error: Can't write books to XML file");
                }
            });
        } catch (Exception e) {
            throw new RuntimeException("Error: Can't get the values from the memory");
        }
    }

    private static void _addChildToElement(Document document, Element parent, String tag, String value) {
        Element e = document.createElement(tag);
        e.setTextContent(value);
        parent.appendChild(e);
    }
}
