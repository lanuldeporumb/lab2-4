package repository;

import domain.Book;
import domain.IDGenerator;
import domain.validators.Validator;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.IOException;
import java.util.*;

/**
 * XML File Repository for the Book object
 */
public class BookXMLRepository implements Repository<Long, Book> {
    private Validator<Book> _validator;
    private String _fileNameXML;
    private Map<Long, Book> _listOfEntities;

    public BookXMLRepository(Validator<Book> _validator, String _fileNameXML) throws Exception {
        this._validator = _validator;
        this._fileNameXML = _fileNameXML;
        this._listOfEntities = new HashMap<>();
        try {
            _listOfEntities = _loadDataFromXML();
        } catch (ParserConfigurationException | IOException | SAXException e) {
            throw new Exception("Error: Can't read file");
        }
    }

    /**
     * Return the book with the given id, from the XML repository. No need to re-read
     * the content of it, because of the implementation of the add function
     * @param id: Long - the id of the book which we need to find
     *            must be not null.
     * @return if the book is found, we return it, otherwise we return null
     */
    @Override
    public Optional<Book> findOne(Long id) {
        return Optional.ofNullable(_listOfEntities.get(id));
    }

    /**
     * Return all the books in the XML repository. No need to re-read the content of it,
     * because of the implementation of the add function
     * @return An array with all the values from the XML file.
     */
    @Override
    public Iterable<Book> findAll() {
        return _listOfEntities.values();
    }

    /**
     * Save a book to the XML file and add it to the XML file
     * @param book: Book - the book need to be saved
     * @return book: Book - if the book can be saved
     *      null otherwise
     * @throws Exception - if the book can't be written to the XML repository.
     */
    @Override
    public Optional<Book> save(Book book) throws Exception {
        if (book == null) {
            throw new IllegalArgumentException("Argument invalid");
        }
        _validator.validate(book);
        _writeToXML(book);
        _listOfEntities.put(book.getID(), book);
        return Optional.of(_listOfEntities.get(book.getID()));
    }

    /**
     * Delete a book from the XML file repository, rewrite the file
     * @param id: Long -  the id of the book which we need to remove
     *            must not be null.
     * @return null if the book still exists in the repository, Book if we failed with removing the book.
     */
    @Override
    public Optional<Book> delete(Long id) {
        _listOfEntities.remove(id);
        _writeEntitiesToXML();
        return Optional.ofNullable(_listOfEntities.get(id));
    }

    /**
     * Update a given book with the new information and re-write the XML file.
     * @param book: Book - the book we need to update.
     * @return book: Book - the new book with the new values, changed
     */
    @Override
    public Optional<Book> update(Book book) {
        // If the book doesn't exist, we can't update it.
        if (findOne(book.getID()).equals(Optional.empty())){
            throw new RuntimeException("Error: Book is not in the repository");
        }
        _listOfEntities.replace(book.getID(), book);
        _writeEntitiesToXML();
        return Optional.of(_listOfEntities.get(book.getID()));
    }

    /**
     * Load the information from the XML file repository
     * @return Map<Long, Book> - The data from the XML file loaded into a map
     * @throws Exception - if the file can't be opened or read
     */
    private Map<Long, Book> _loadDataFromXML() throws Exception {
        DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
        Document xmlDocument = documentBuilder.parse(_fileNameXML);
        // Read the root of the XML file
        Element root = xmlDocument.getDocumentElement();
        NodeList children = root.getChildNodes();
        Map<Long, Book> bookList = new HashMap<>();
        for (int i = 0; i < children.getLength(); i++) {
            Node child = children.item(i);
            if (child instanceof Element) {
                Element bookElement = ((Element) child);
                String author = bookElement.getChildNodes().item(0).getTextContent();
                String name = bookElement.getChildNodes().item(1).getTextContent();
                int price = Integer.parseInt(bookElement.getChildNodes().item(2).getTextContent());
                // Create the book
                Book book = new Book(IDGenerator.GenerateID(), name, author, price);
                bookList.put(book.getID(), book);
            }
        }
        return bookList;
    }

    /**
     * Write the given book to the XML file.
     * @param book: Book - the book to add to the XML file
     * @throws Exception - if the book can't be written to the file
     */
    private void _writeToXML(Book book) throws Exception {
        // Initiate the elements needed to create a builder for the XML file
        DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
        Document xmlDocument = documentBuilder.parse(_fileNameXML);
        // Read the root of the xml file
        Element root = xmlDocument.getDocumentElement();
        // Create the element of type Book
        Element bookToSave = xmlDocument.createElement("book");
        _addChildToElement(xmlDocument, bookToSave, "title", book.getName());
        _addChildToElement(xmlDocument, bookToSave, "author", book.getAuthor());
        _addChildToElement(xmlDocument, bookToSave, "price", String.valueOf(book.getPrice()));
        // Save the book in the root of the XML file
        root.appendChild(bookToSave);
        // The transformer will create the XML file with the given data
        Transformer transformer = TransformerFactory.newInstance().newTransformer();
        transformer.transform(new DOMSource(root), new StreamResult(_fileNameXML));
    }

    private void _writeEntitiesToXML() {
        _listOfEntities.values().forEach(book -> {
            try {
                _writeToXML(book);
            } catch (Exception e) {
                throw new RuntimeException("Error: Can't write books to XML file");
            }
        });
    }

    private static void _addChildToElement(Document document, Element parent, String tag, String value) {
        Element e = document.createElement(tag);
        e.setTextContent(value);
        parent.appendChild(e);
    }

}
