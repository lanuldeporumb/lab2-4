package domain;

import java.util.Objects;

public class Book extends BaseEntity<Long> {
    private String name;
    private String author;
    private int price;

    /**
     * Basic Contructor
     */
    public Book(){

    }

    /**
     * Constructor with parameters
     * @param id book id
     * @param name book name
     * @param author author name
     * @param price book price
     */
    public Book(Long id, String name, String author, int price){
        this.setID(id);
        this.name = name;
        this.author = author;
        this.price = price;
    }

    /**
     * Returns the name of the book
     * @return String book name
     */
    public String getName(){
        return name;
    }

    /**
     * Sets the name of the book to the given String
     * @param name given name
     */
    public void setName(String name){
        this.name = name;
    }

    /**
     * Returns the name of the author
     * @return String author name
     */
    public String getAuthor(){
        return author;
    }

    /**
     * Sets the name of the author to the given String
     * @param author given name
     */
    public void setAuthor(String author){
        this.author = author;
    }

    /**
     * Returns the price of the book
     * @return int price
     */
    public int getPrice(){
        return price;
    }

    /**
     * Sets the price of the book
     * @param price given price
     */
    public void setPrice(int price){
        this.price = price;
    }

    /**
     * Returns the Book's characteristics as a String
     * @return String
     */
    @Override
    public String toString() {
        return "Book{" +
                "ID: " + this.getID() +
                " | " +
                "Name: " + name +
                " | " +
                "Author: " + author +
                " | " +
                "Price: " + price + " RON" +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Book book = (Book) o;
        return getID().equals(((Book) o).getID());
    }

    @Override
    public int hashCode() {

        return Objects.hash(name, author, price);
    }
}
