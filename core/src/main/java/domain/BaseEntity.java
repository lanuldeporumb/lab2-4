package domain;

public class BaseEntity<ID> {
    private ID id;

    /**
     * Returns the ID of the entity
     * @return ID
     */
    public ID getID() {
        return id;
    }

    /**
     * Sets the ID of the entity
     * @param id ID to set to
     */
    public void setID(ID id) {
        this.id = id;
    }

    /**
     * Returns the entity as a String containing its ID
     * @return String
     */
    @Override
    public String toString() {
        return "BaseEntity{" +
                "ID: " + id +
                '}';
    }
}
