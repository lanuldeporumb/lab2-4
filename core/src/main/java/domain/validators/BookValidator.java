package domain.validators;

import domain.Book;

public class BookValidator implements Validator<Book>{
    /**
     * Validates entities of type Book
     * @param entity entity to be validated
     * @throws ValidatorException if any exceptional situations arise
     */
    public void validate(Book entity) throws ValidatorException{
        if (entity == null) {
            throw new ValidatorException("entity cannot be null");
        }

        if (entity.getID() <= 0) {
            throw new ValidatorException("id cannot be null or negative");
        }

        if (entity.getPrice() <= 0) {
            throw new ValidatorException("price cannot be null or negative");
        }
    }
}