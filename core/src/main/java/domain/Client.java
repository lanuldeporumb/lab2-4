package domain;

import java.util.Objects;

public class Client extends BaseEntity<Long> {
    private String _name;
    private String _address;

    public Client(Long id, String _name, String _address) {
        this.setID(id);
        this._name = _name;
        this._address = _address;
    }

    /**
     * Get the name of the client
     * @return :String - the name of the client
     */
    public String get_name() {
        return _name;
    }

    /**
     * Set the new name of the client
     * @param _name: String - the new name
     */
    public void set_name(String _name) {
        this._name = _name;
    }

    /**
     * Get the address of the client
     * @return :String - the address
     */
    public String get_address() {
        return _address;
    }

    /**
     * Set the address of the client
     * @param _address: String - the new address of the client
     */
    public void set_address(String _address) {
        this._address = _address;
    }

    /**
     * Check if the given object is equal to the current client
     * @param o: Object - the object to check
     * @return Boolean - the status of the object
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Client client = (Client) o;
        return Objects.equals(get_name(), client.get_name()) &&
                Objects.equals(get_address(), client.get_address());
    }

    /**
     * Get the hash code of the object
     * @return int - the hash code of the client
     */
    @Override
    public int hashCode() {
        return Objects.hash(get_name(), get_address());
    }

    /**
     * Return the string object of the client
     * @return String - the client data
     */
    @Override
    public String toString() {
        return "Client{" +
                "_name='" + _name + '\'' +
                ", _address='" + _address + '\'' +
                '}';
    }
}
