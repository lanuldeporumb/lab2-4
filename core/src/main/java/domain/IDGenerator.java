package domain;

public class IDGenerator {
    private static Long id = 0L;

    public static Long GenerateID() {
        return ++id;
    }
}
