package domain;

import org.junit.Test;

import static org.junit.Assert.*;

public class IDGeneratorTest {

    @Test
    public void generateID() {
        assertTrue("Should obtain id equal to 1", IDGenerator.GenerateID().equals(1L));
        assertFalse("Should not obtain id equal to 1", IDGenerator.GenerateID().equals(2L));
        assertTrue("Should obtain id equal to 3", IDGenerator.GenerateID().equals(3L));
    }
}