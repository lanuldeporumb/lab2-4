package ui;

import domain.Book;
import domain.IDGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import service.BookService;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;


/**
 * @author Fovas Denis.
 */
@Component
public class Console {
    @Autowired
    private BookService _bookService;

    public Console(BookService _bookService) {
        this._bookService = _bookService;
    }

    /**
     * Prints a text-based menu
     */
    private void printMenu(){
        String string = "Options:\n";
        string += "1 - List all books.\n";
        string += "2 - Add Book.\n";
        string += "3 - Delete Book.\n";
        string += "4 - Update Book.\n";
        string += "0 - Exit.\n";

        System.out.print(string);
    }

    /**
     * Will run the console. First it will add books until the user will stop
     * the execution with an error, then will display all books.
     */
    public void runConsole() {
        BufferedReader bf = new BufferedReader(new InputStreamReader(System.in));
        boolean keepAlive = true;
        int command;
        while (keepAlive){
            this.printMenu();
            System.out.println("Input an option: ");
            try {
                command = Integer.valueOf(bf.readLine());
            } catch (IOException e) {
                System.out.println("Invalid data inserted.");
                return;
            }
            switch(command){
                case 0: keepAlive = false; break;
                case 1: this._printAllBooks(); break;
                case 2: this._addBook(); break;
                case 3: this._deleteBook(); break;
                case 4: this._updateBook(); break;
                default: System.out.println("Command unavailable."); break;
            }
        }
    }

    /**
     * Display all books from the repository.
     */
    private void _printAllBooks() {
        try {
            for (Book book :
                    _bookService.getAllBooks()) {
                System.out.println(book);
            }
        } catch (Exception e) {
            System.out.println("Error! Please check the message and then continue: ");
            System.out.println(e.getMessage());
        }
    }

    /**
     * Creates a book with the data from the user, read from the System.in stream.
     * If a value is incorrect it will display an error message.
     * @return Book with the values from the user or null
     */
    private Book _readBook() {
        System.out.println("Please insert the data in the following order: {name, author, price}");
        BufferedReader bf = new BufferedReader(new InputStreamReader(System.in));
        try {
            Long id = IDGenerator.GenerateID();
            String name = bf.readLine();
            String author = bf.readLine();
            int price = Integer.parseInt(bf.readLine());

            return new Book(id, name, author, price);
        } catch (IOException e) {
            System.out.println("Invalid data inserted.");
            return null;
        }
    }

    /**
     * Creates a new book with information required for updating
     * @return Book with ID of an existing book and new desired values for name, author and price
     */
    private Book _readBookForUpdate() {
        BufferedReader bf = new BufferedReader(new InputStreamReader(System.in));
        Long id = null;
        try {
            System.out.println("Please insert the ID of the book:");
            id = Long.valueOf(bf.readLine());
            System.out.println("Insert the new name: (empty means leave unchanged)");
            String name = bf.readLine();
            System.out.println("Insert the new author: (empty means leave unchanged)");
            String author = bf.readLine();
            System.out.println("Insert the new price: (-1 means leave unchanged)");
            int price = Integer.parseInt(bf.readLine());

            return new Book(id, name, author, price);
        } catch (IOException e) {
            System.out.println("Invalid data inserted.");
            return null;
        }
    }

    /**
     * Adds a book from the user input into the repository.
     * If the input is invalid, we will exit the function.
     * If we have an error, we will display it and exit the function.
     */
    private void _addBook() {
        Book createdBook = _readBook();
        try {
            _bookService.addBook(createdBook);
        } catch (Exception e) {
            System.out.println("!Error: " + e.getMessage());
        }
    }

    /**
     * Deletes the book with the given ID from the repository
     * Errors or invalid inputs will display a message
     */
    private void _deleteBook(){
        System.out.println("Please insert the ID of the book: ");
        BufferedReader bf = new BufferedReader(new InputStreamReader(System.in));
        Long id = null;
        try{
            id = Long.valueOf(bf.readLine());
        } catch (IOException e) {
            System.out.println("Invalid data inserted.");
        }
        try{
            _bookService.deleteBook(id);
        } catch (Exception e) {
            System.out.println("!Error: " + e.getMessage());
        }
    }

    /**
     * Constructs a new book with desired update values
     * Updates a book from the repository by calling the update() function with required values
     * Errors display a message
     */
    private void _updateBook(){
        Book readBook = _readBookForUpdate();
        try{
            _bookService.update(readBook.getID(), readBook.getName(), readBook.getAuthor(), readBook.getPrice());
        } catch (Exception e) {
            System.out.println("!Error: " + e.getMessage());
        }

    }
}
