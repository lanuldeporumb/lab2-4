import domain.Book;
import domain.validators.BookValidator;
import domain.validators.Validator;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import repository.BookXMLRepository;
import repository.InMemoryRepository;
import repository.Repository;
import ui.Console;

@SuppressWarnings("WeakerAccess")
class Main {
    public static void main(String args[]) /*throws Exception*/ {
//        Repository<Long, Book> xmlRepository = null;
//        // Create the validator
//        Validator<Book> validator = new BookValidator();
//        // Create the repository
////        CreateRepository<Long, Book> repository = new InMemoryRepository<>(validator);
//        try {
//            xmlRepository = new BookXMLRepository(validator, "data/books.xml");
//        } catch (Exception e) {
//            e.printStackTrace();

//        Repository<Long, Book> repository = new DBRepository(validator,
//                "jdbc:postgresql://localhost:5432/postgres",
//                "postgres",
//                "postgres");
//        try {
//            Repository<Long, Book> xmlBookRepo = new BookXMLRepository(validator, "book.xml");
//        } catch (Exception e) {
//            System.out.println(e.getMessage());
//        }
//        // Create the bookService
//        BookService bookService = new BookService(xmlRepository);
//        // Create console
//        Console console = new Console(bookService);
//        // Run the console application
//        console.runConsole();

        AnnotationConfigApplicationContext context =
            new AnnotationConfigApplicationContext("java.config");

        Console console=context.getBean(Console.class);
        //console.runConsole();
    }
}
