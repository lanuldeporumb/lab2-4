drop table books;
create table books (
  b_id SERIAL PRIMARY KEY,
  book_name VARCHAR(50),
  book_author VARCHAR(50),
  price INTEGER);

insert into books (book_name, book_author, price) values
  ('Ana are mere', 'Sthepen', 32),
  ('Ana nu mai are mere', 'Un hot', 420),
  ('Buy my new chair', 'Pewds', 399);

SELECT * FROM books;

drop table clients;
create table clients (
  c_id SERIAL PRIMARY KEY,
  client_name VARCHAR(50),
  client_street VARCHAR(50)
);

insert into clients (client_name, client_street) values
  ('Georgel', 'Strada valuare'),
  ('Pewds', 'Strret BRo');

SELECT * FROM clients;