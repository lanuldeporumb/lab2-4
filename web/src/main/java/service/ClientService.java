package service;

import domain.Client;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import repository.Repository;

import java.util.Optional;

public class ClientService {
    private static final Logger LOG = LoggerFactory.getLogger(BookService.class);

    private Repository<Long, Client> _repository;

    public ClientService(Repository<Long, Client> _repository) {
        this._repository = _repository;
    }

    /**
     * Remove a client based on it's id
     * @param id:Long - the id of the client
     * @throws Exception - If the client can't be removed
     */
    public void removeClient(Long id) throws Exception {
        LOG.trace("removeClient function initiated");
        try {
            _repository.deleteById(id);
        } catch (Exception e) {
            LOG.trace("removeClient function -- exception caught");
            throw new Exception("Error: Can't remove the entity!\n" + e.getMessage());
        }
        LOG.trace("removeClient function finished");
    }

    /**
     * Save a client to the database
     * @param client: Client - the client to be saved
     * @throws Exception - If the client can't be saved
     */
    public void saveClient(Client client) throws Exception {
        LOG.trace("saveClient function initiated");
        _repository.save(client);
        LOG.trace("removeClient function finished");
    }

    /**
     * Get a list of all the clients
     * @return Iterable<Client> - the list of clients
     * @throws Exception - if the list can't be created
     */
    public Iterable<Client> getAllClients() throws Exception {
        LOG.trace("getAllClients function initiated");
        LOG.trace("getAllClients function finished");
        return _repository.findAll();
    }

    /**
     * Find a client based on it's id
     * @param id:Long - the id of the client
     * @return Client
     * @throws Exception - If the client can't be found
     */
    public Optional<Client> findOne(Long id) throws Exception {
        LOG.trace("findOne function initiated");
        LOG.trace("findOne function finished");
        return _repository.findById(id);
    }

    /**
     * Update the client with it's new data. The id will be the same
     * @param id:Long - the id of the client
     * @param newName:String - the new name of the client
     * @param newAddress:String - the new address of the client
     * @return Client - the new client with the new data
     * @throws Exception - If the update can't be created
     */
    public Optional<Client> updateClient(Long id, String newName, String newAddress) throws Exception {
        LOG.trace("updateClient function initiated");
        Optional<Client> client = _repository.findById(id);
        Client newClient = new Client(client.get().getID(),
                newName.equals("") ? client.get().get_name() : newName,
                newAddress.equals("")? client.get().get_address() : newAddress);
        LOG.trace("updateClient function finished");
        return _repository.update(newClient);
    }
}
