package service;

import domain.Book;
import domain.Client;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import repository.Repository;

import java.util.Optional;

public class ClientBookService {
    private static final Logger LOG = LoggerFactory.getLogger(BookService.class);

    private Repository<Long, Client> _clientRepository;
    private Repository<Long, Book> _bookRepository;

    public ClientBookService(Repository<Long, Client> _clientRepository, Repository<Long, Book> _bookRepository) {
        this._clientRepository = _clientRepository;
        this._bookRepository = _bookRepository;
    }

    public String buyBook(Long book_id, Long client_id) throws Exception {
        LOG.trace("buyBook function initiated");
        try {
            Optional<Book> b = _bookRepository.findOne(book_id);
            _bookRepository.delete(book_id);
            Optional<Client> cl = _clientRepository.findOne(client_id);
            LOG.trace("updateClient function finished");
            return "Client " + cl.get().toString() + " has bought the book " + b.get().toString();
        } catch (Exception e) {
            LOG.trace("buyBook function -- exception caught");
            throw new Exception("Can't buy the book. Maybe it's not in stock?");
        }
    }
}
