package service;

import domain.Book;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.xml.sax.SAXException;
import repository.BookJPARepository;
import repository.Repository;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.util.Set;
import java.util.stream.Collectors;
import org.springframework.stereotype.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import java.util.stream.StreamSupport;


@Service
public class BookService {
    private static final Logger LOG = LoggerFactory.getLogger(BookService.class);

    @Autowired
    private Repository<Long, Book> _repository;


    /**
     * Constructor for the service.
     * @param _repository - repository of the Book Service
     */
    public BookService(Repository<Long, Book> _repository) {
        this._repository = _repository;
    }

    /**
     * Will add a book to the repository.
     * @param book: Book
     * @throws Exception -  if it can't save it
     */
    public void addBook(Book book) throws Exception {
        LOG.trace("addBook function initiated");
        _repository.save(book);
        LOG.trace("addBook function finished");
    }

    /**
     * Gets all books from the repository.
     * @return Set<Book> with all the books
     */
    public Set<Book> getAllBooks() throws Exception {
        LOG.trace("getAllBooks function initiated");
        Iterable<Book> books = _repository.findAll();
        LOG.trace("getAllBooks function finished");
        return StreamSupport.stream(books.spliterator(), false).collect(Collectors.toSet());
    }

    /**
     * Will search for a book in the repository based on an id
     * @param id: Long
     * @return Book the book found in the repository
     */
    public Book findOne(Long id) throws Exception{
        LOG.trace("findOne function initiated");
        Book book = _repository.findById(id).get();

        LOG.trace("findOne function finished");
        return book;
    }

    /**
     * Will delete a book based on an id
     * @param id: Long
     */
    public void deleteBook(Long id) throws Exception {
        LOG.trace("deleteBook function initiated");
        _repository.deleteById(id);
        LOG.trace("deleteBook function finished");
    }

    /**
     * Will update a book with the changed data, except the id of the book.
     * The id must be always the one from a book in repository.
     * If the string values are equal to "", then they will not be updated.
     * If the new price is -1, then it will not be updated.
     * @param id: Long - the id of the book we need to update
     * @param name: String - the new name of the book
     * @param author: String - the new author of the book
     * @param price: int - the new price of the book
     * @return Book - the new book updated with the new data
     * @throws Exception - throws Exception if the book is not validated right in the repository.
     */
    @Transactional
    public Book update(Long id, String name, String author, int price) throws Exception {
        LOG.trace("Book -- update function initiated");
        Book bookToUpdate = _repository.findById(id).get();
        Book newBook = new Book(bookToUpdate.getID(),
                            (name.equals("")) ? bookToUpdate.getName() : name,
                            author.equals("") ? bookToUpdate.getAuthor() : author,
                            price == -1 ? bookToUpdate.getPrice() : price);
        _repository.update(newBook);
        LOG.trace("Book -- update function finished");

        return newBook;
    }
}
